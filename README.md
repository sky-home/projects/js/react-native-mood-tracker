# Mood Tracker

This is a React Native application wrapped with [expo](https://www.expo.dev).

I made this application base on Frontend Masters course. [frontend masters react native](https://frontendmasters.com/courses/intermediate-react-native/)

## How to use

To use this application, first run this following commands.

```bash
yarn # or npm install
```

then

```bash
yarn start
```

You good to go.

## Screenshots

![home](./sreenshots/1.jpg)
![history](./sreenshots/2.jpg)

import "react-native-gesture-handler"
import { NavigationContainer } from "@react-navigation/native"
import React from "react"
import { AppProvider } from "./src/providers/App.provider"
import { BottomTabsNavigator } from "./src/screens/BottomTabs.navigator"

export default function App() {
  return (
    <AppProvider>
      <NavigationContainer>
        <BottomTabsNavigator />
      </NavigationContainer>
    </AppProvider>
  )
}

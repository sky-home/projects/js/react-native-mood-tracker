import React from "react"
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs"
import { Home } from "./Home.screen"
import { History } from "./History.screen"
import { Analytics } from "./Analytics.screen"
import {
  AnalyticsIcon,
  HistoryIcon,
  HomeIcon,
} from "../components/Icons.component"
import { theme } from "../theme"
import { useAppContext } from "../providers/App.provider"

const BottomTabs = createBottomTabNavigator()

export const BottomTabsNavigator: React.FC = () => {
  const appContext = useAppContext()
  const fontsLoaded = appContext.handleFonts()

  if (!fontsLoaded) return null

  return (
    <BottomTabs.Navigator
      screenOptions={({ route }) => ({
        headerTitleStyle: {
          fontFamily: theme.fontFamilyRegular,
        },
        tabBarActiveTintColor: theme.colorBlue,
        tabBarInactiveTintColor: theme.colorGrey,
        tabBarShowLabel: false,
        tabBarIcon: ({ color, size }) => {
          if (route.name === "Home")
            return <HomeIcon color={color} size={size} />
          if (route.name === "History")
            return <HistoryIcon color={color} size={size} />
          if (route.name === "Analytics")
            return <AnalyticsIcon color={color} size={size} />

          return null
        },
      })}
    >
      <BottomTabs.Screen
        name='Home'
        component={Home}
        options={{ title: "Today's Mood" }}
      />
      <BottomTabs.Screen
        name='History'
        component={History}
        options={{ title: "Past Moods" }}
      />
      <BottomTabs.Screen
        name='Analytics'
        component={Analytics}
        options={{ title: "Fancy Charts" }}
      />
    </BottomTabs.Navigator>
  )
}

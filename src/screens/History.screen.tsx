import React from "react"
import { ScrollView } from "react-native"
import { MoodItemRow } from "../components/MoodItemRow.component"
import { useAppContext } from "../providers/App.provider"

export const History: React.FC = () => {
  const appContext = useAppContext()
  return (
    <ScrollView>
      {appContext.moodList.slice().reverse().map((mood) => (
        <MoodItemRow key={mood.timestamp} item={mood} />
      ))}
    </ScrollView>
  )
}

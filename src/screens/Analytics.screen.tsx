import { groupBy } from "lodash"
import React from "react"
import { View, Text } from "react-native"
import { VictoryPie } from "victory-native"
import { useAppContext } from "../providers/App.provider"
import { theme } from "../theme"

export const Analytics: React.FC = () => {
  const appContext = useAppContext()

  const data = Object.entries(groupBy(appContext.moodList, "mood.emoji")).map(
    ([key, value]) => ({
      x: key,
      y: value.length,
    })
  )

  return (
    <View>
      <Text>Analytics</Text>
      <VictoryPie
        labelRadius={80}
        radius={150}
        innerRadius={50}
        colorScale={[
          theme.colorPurple,
          theme.colorLavender,
          theme.colorBlue,
          theme.colorGrey,
          theme.colorWhite,
        ]}
        data={data}
        style={{ labels: { fontSize: 30 } }}
      />
    </View>
  )
}

import React from "react"
import { StyleSheet, View, Text, Pressable, Image } from "react-native"
import { MoodOptionType } from "../types"
import { theme } from "../theme"
import { useAppContext } from "../providers/App.provider"

const imageSrc = require("../../assets/butterflies.png")

import Reanimated, {
  useAnimatedStyle,
  withTiming,
} from "react-native-reanimated"

const ReanimatedPressable = Reanimated.createAnimatedComponent(Pressable)

const moodOptions: MoodOptionType[] = [
  { emoji: "🧑‍💻", description: "studious" },
  { emoji: "🤔", description: "pensive" },
  { emoji: "😊", description: "happy" },
  { emoji: "🥳", description: "celebratory" },
  { emoji: "😤", description: "frustrated" },
]

type MoodPickerProps = {
  onSelect: (mood: MoodOptionType) => void
}

export const MoodPicker: React.FC<MoodPickerProps> = ({ onSelect }) => {
  const [selectedMood, setSelectedMood] = React.useState<MoodOptionType>()
  const [hasSelected, sethasSelected] = React.useState(false)
  const appContext = useAppContext()
  const fontsLoaded = appContext.handleFonts()

  const buttonStyle = useAnimatedStyle(
    () => ({
      opacity: selectedMood ? withTiming(1) : withTiming(0.5),
      transform: [{ scale: selectedMood ? withTiming(1) : 0.8 }],
    }),
    [selectedMood]
  )

  const handleSelect = React.useCallback(() => {
    if (selectedMood) {
      onSelect(selectedMood)
      setSelectedMood(undefined)
      sethasSelected(true)
    }
  }, [selectedMood, onSelect])

  if (hasSelected) {
    return (
      <View style={styles.container}>
        <Image style={styles.image} source={imageSrc} />
        <Pressable style={styles.button} onPress={() => sethasSelected(false)}>
          <Text style={styles.buttonText}>Back</Text>
        </Pressable>
      </View>
    )
  }

  if (!fontsLoaded) return null

  return (
    <View style={styles.container}>
      <Text style={styles.heading}>How are you right now?</Text>
      <View style={styles.moodList}>
        {moodOptions.map((opt) => (
          <View key={opt.emoji}>
            <Pressable
              style={[
                styles.moodItem,
                selectedMood?.emoji === opt.emoji
                  ? styles.selectedMoodItem
                  : undefined,
              ]}
              onPress={() => setSelectedMood(opt)}
            >
              <Text>{opt.emoji}</Text>
            </Pressable>
            <Text style={styles.descText}>
              {selectedMood?.emoji === opt.emoji ? opt.description : undefined}
            </Text>
          </View>
        ))}
      </View>
      <ReanimatedPressable
        style={[styles.button, buttonStyle]}
        onPress={handleSelect}
      >
        <Text style={styles.buttonText}>Submit</Text>
      </ReanimatedPressable>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    height: 250,
    margin: 10,
    padding: 20,
    borderWidth: 2,
    borderColor: theme.colorPurple,
    borderRadius: 15,
    justifyContent: "space-between",
    backgroundColor: "rgba(0, 0, 0, 0.2)",
  },
  heading: {
    fontSize: 20,
    letterSpacing: 1,
    textAlign: "center",
    color: theme.colorWhite,
    fontFamily: theme.fontFamilyBold,
  },
  button: {
    padding: 10,
    backgroundColor: theme.colorPurple,
    width: 150,
    borderRadius: 20,
    alignSelf: "center",
  },
  buttonText: {
    color: theme.colorWhite,
    textAlign: "center",
    fontFamily: "Kalam-Bold",
  },
  moodList: {
    flexDirection: "row",
    justifyContent: "space-between",
    padding: 20,
  },
  moodItem: {
    height: 60,
    width: 60,
    borderRadius: 30,
    justifyContent: "center",
    alignItems: "center",
  },
  selectedMoodItem: {
    borderWidth: 2,
    backgroundColor: theme.colorPurple,
    borderColor: theme.colorWhite,
  },
  descText: {
    color: theme.colorPurple,
    textAlign: "center",
    fontSize: 10,
    fontFamily: "Kalam-Bold",
  },
  image: {
    alignSelf: "center",
  },
})

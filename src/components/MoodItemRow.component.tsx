import React from "react"
import {
  StyleSheet,
  View,
  Text,
  Pressable,
  LayoutAnimation,
} from "react-native"
import { MoodOptionWithTimestamp } from "../types"
import { format } from "date-fns"
import { theme } from "../theme"
import { useAppContext } from "../providers/App.provider"
import Reanimated, {
  runOnJS,
  useAnimatedGestureHandler,
  useAnimatedStyle,
  useSharedValue,
  withTiming,
} from "react-native-reanimated"
import {
  GestureHandlerRootView,
  PanGestureHandler,
} from "react-native-gesture-handler"

type MoodItemRowProps = {
  item: MoodOptionWithTimestamp
}

const MAXSWIPE = 80

export const MoodItemRow: React.FC<MoodItemRowProps> = ({ item }) => {
  const appContext = useAppContext()
  const offset = useSharedValue(0)

  const handleDelete = React.useCallback(() => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    appContext.handleDeleteMood(item)
  }, [appContext, item])

  const handleDeleteDelay = React.useCallback(() => {
    setTimeout(() => {
      handleDelete()
    }, 500)
  }, [])

  const animatedStyle = useAnimatedStyle(() => ({
    transform: [{ translateX: offset.value }],
  }))

  const onGestureEvent = useAnimatedGestureHandler({
    onActive: (event) => {
      const xVal = Math.floor(event.translationX)
      offset.value = xVal
    },
    onEnd: (event) => {
      if (Math.abs(event.translationX) > MAXSWIPE) {
        offset.value = withTiming(1000 * Math.sign(event.translationX))
        runOnJS(handleDeleteDelay)()
      } else offset.value = withTiming(0)
    },
  })

  return (
    <GestureHandlerRootView>
      <PanGestureHandler onGestureEvent={onGestureEvent}>
        <Reanimated.View style={[styles.moodItem, animatedStyle]}>
          <View style={styles.iconAndDescription}>
            <Text style={styles.moodValue}>{item.mood.emoji}</Text>
            <Text style={styles.moodDescription}>{item.mood.description}</Text>
          </View>
          <Text style={styles.moodDate}>
            {format(new Date(item.timestamp), "dd MMM, yyyy 'at' hh:mmaaa")}
          </Text>
          <Pressable onPress={handleDelete}>
            <Text style={styles.deleteText}>Delete</Text>
          </Pressable>
        </Reanimated.View>
      </PanGestureHandler>
    </GestureHandlerRootView>
  )
}

const styles = StyleSheet.create({
  moodValue: {
    textAlign: "center",
    fontSize: 40,
    marginRight: 10,
  },
  moodDate: {
    textAlign: "center",
    color: theme.colorLavender,
    fontFamily: theme.fontFamilyRegular,
  },
  moodItem: {
    backgroundColor: "white",
    marginBottom: 10,
    padding: 10,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  moodDescription: {
    fontSize: 18,
    color: theme.colorPurple,
    fontFamily: theme.fontFamilyBold,
  },
  iconAndDescription: {
    flexDirection: "row",
    alignItems: "center",
  },
  deleteText: {
    fontFamily: theme.fontFamilyBold,
    color: theme.colorBlue,
  },
})

import React from "react"
import AsyncStorage from "@react-native-async-storage/async-storage"
import { MoodOptionType, MoodOptionWithTimestamp } from "../types"
import { useFonts } from "expo-font"

const appKey = "my-app-data"

type AppData = {
  moods: MoodOptionWithTimestamp[]
}

const fonts = {
  "Kalam-Bold": require("../../assets/fonts/Kalam-Bold.ttf"),
  "Kalam-Regular": require("../../assets/fonts/Kalam-Regular.ttf"),
  "Kalam-Light": require("../../assets/fonts/Kalam-Light.ttf"),
}

const setAppData = async (newData: AppData): Promise<void> => {
  try {
    await AsyncStorage.setItem(appKey, JSON.stringify(newData))
  } catch {}
}

const getAppData = async (): Promise<AppData | null> => {
  try {
    const result = await AsyncStorage.getItem(appKey)
    if (result) return JSON.parse(result)
  } catch {}

  return null
}

type AppContextType = {
  moodList: MoodOptionWithTimestamp[]
  handleSelectMood: (mood: MoodOptionType) => void
  handleFonts: () => boolean
  handleDeleteMood: (mood: MoodOptionWithTimestamp) => void
}

const defaultValue = {
  moodList: [],
  handleSelectMood: () => {},
  handleFonts: () => false,
  handleDeleteMood: () => {}
}

const AppContext = React.createContext<AppContextType>(defaultValue)

export const AppProvider: React.FC = ({ children }) => {
  const [moodList, setmoodList] = React.useState<MoodOptionWithTimestamp[]>([])
  const handleFonts = React.useCallback(() => {
    const [fontsLoaded] = useFonts(fonts)
    return fontsLoaded
  }, [])

  const handleSelectMood = React.useCallback((mood: MoodOptionType) => {
    setmoodList((current) => {
      const newValue = [...current, { mood, timestamp: Date.now() }]

      setAppData({ moods: newValue })

      return newValue
    })
  }, [])

  const handleDeleteMood = React.useCallback(
    (mood: MoodOptionWithTimestamp) => {
      setmoodList((current) => {
        const newMoodList = current.filter(
          (val) => val.timestamp !== mood.timestamp
        )
        setAppData({ moods: newMoodList })
        return newMoodList
      })
    },
    []
  )

  React.useEffect(() => {
    const getData = async () => {
      const data = await getAppData()
      if (data) setmoodList(data.moods)
    }

    getData()
  }, [])

  return (
    <AppContext.Provider value={{ moodList, handleSelectMood, handleFonts, handleDeleteMood }}>
      {children}
    </AppContext.Provider>
  )
}

export const useAppContext = () => React.useContext(AppContext)
